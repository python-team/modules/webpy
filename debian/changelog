webpy (1:0.62-5) unstable; urgency=medium

  * Team upload.
  * (Build-)depend on python3-legacy-cgi (closes: #1082337).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Sat, 16 Nov 2024 22:34:58 +0100

webpy (1:0.62-4) unstable; urgency=medium

  * Fix debian/watch

 -- Martin <debacle@debian.org>  Sat, 25 Feb 2023 22:14:11 +0000

webpy (1:0.62-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 31 Oct 2022 17:51:13 +0000

webpy (1:0.62-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Contact.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 02:21:05 +0100

webpy (1:0.62-1) unstable; urgency=medium

  * New upstream bug fix release
  * Bump debhelper-compat to 13
  * Update debian/watch

 -- Martin <debacle@debian.org>  Thu, 16 Jun 2022 22:05:12 +0000

webpy (1:0.61-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Sandro Tosi <morph@debian.org>  Thu, 16 Jun 2022 14:30:58 -0400

webpy (1:0.61-1) unstable; urgency=medium

  * New upstream release

 -- Martin <debacle@debian.org>  Sat, 05 Sep 2020 19:59:22 +0000

webpy (1:0.40-2) unstable; urgency=medium

  * Remove build-dependency on RC buggy python3-mysql.connector

 -- W. Martin Borgert <debacle@debian.org>  Mon, 07 Oct 2019 22:29:05 +0000

webpy (1:0.40-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ W. Martin Borgert ]
  * New upstream release, also Closes: #940106

 -- W. Martin Borgert <debacle@debian.org>  Sun, 29 Sep 2019 12:28:32 +0000

webpy (1:0.39+20181101-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ W. Martin Borgert ]
  * New upstream snapshot, support Python 3.7 (Closes: #920023)
  * Drop Python 2 package, because of dependency on python3-cheroot
    (Closes: #584924)

 -- W. Martin Borgert <debacle@debian.org>  Tue, 22 Jan 2019 22:36:38 +0000

webpy (1:0.38+20170615-1) unstable; urgency=medium

  * New upstream snapshot, support Python 3 (Closes: #872825)
  * Add documentation (Closes: #570349)
  * Use debhelper 10 and bump standards version to 4.0.1, no changes

 -- W. Martin Borgert <debacle@debian.org>  Mon, 21 Aug 2017 18:56:11 +0000

webpy (1:0.38-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ W. Martin Borgert ]
  * New upstream release 0.38
  * Bump standards version to 3.9.8, no changes

 -- W. Martin Borgert <debacle@debian.org>  Sat, 23 Jul 2016 08:36:52 +0000

webpy (1:0.37+20120626-1) unstable; urgency=low

  * New upstream release. Used git snapshot to get changelog and license.
    No relevant change since 1:0.36+20120529-1.

 -- W. Martin Borgert <debacle@debian.org>  Thu, 28 Jun 2012 20:58:48 +0000

webpy (1:0.36+20120529-1) unstable; urgency=low

  * New upstream code, however not yet 0.37 (Closes: #638827).
  * Use dh instead of verbose debian/rules.

 -- W. Martin Borgert <debacle@debian.org>  Sat, 02 Jun 2012 22:05:16 +0000

webpy (1:0.34-2) unstable; urgency=low

  * Upstream changelog is now debian/CHANGELOG, not
    debian/NEWS

 -- W. Martin Borgert <debacle@debian.org>  Mon, 05 Apr 2010 14:09:58 +0000

webpy (1:0.34-1) unstable; urgency=low

  * New upstream release

 -- W. Martin Borgert <debacle@debian.org>  Sat, 20 Mar 2010 22:45:57 +0000

webpy (1:0.33-1) unstable; urgency=low

  * New upstream release
  * Jakub Wilk <ubanus@users.sf.net>  Fri, 13 Nov 2009 14:15:41 +0100:
    - Add watch file.

 -- W. Martin Borgert <debacle@debian.org>  Mon, 07 Dec 2009 19:32:18 +0000

webpy (1:0.32+dak1-1) unstable; urgency=low

  * New upstream release
  * Change version numbering scheme to upstream method (closes: #517416),
    thanks to Savvas Radevic
  * Relax and widen dependency on database (closes: #511367), thanks to
    Andrew Malcolmson and Piotr Ożarowski
  * Unfortunately, Kai has to orphan the package. Thanks to Kai for his
    work on the package and for blessing takeover by python-modules-team
  * fake upstream version number to make dak happy after broken upload

 -- W. Martin Borgert <debacle@debian.org>  Wed, 26 Aug 2009 22:32:36 +0000

webpy (0.310-1) unstable; urgency=low

  * New upstream release
  * Closes: #509725: Update to fixes a bug with wsgi and SSL
  * https://bugs.edge.launchpad.net/ubuntu/+source/webpy/+bug/311225

 -- Kai Hendry <hendry@iki.fi>  Fri, 26 Dec 2008 11:53:39 +0000

webpy (0.300-1) unstable; urgency=low

  * New upstream release
  * Upgrade guide: http://webpy.org/docs/0.3/upgrade

 -- Kai Hendry <hendry@iki.fi>  Tue, 09 Dec 2008 22:23:38 +0000

webpy (0.230-1) unstable; urgency=low

  * New Upstream Version
  * See http://webpy.org/changes
  * Added links to Git repo and homepage in control
  * Removed unrequired dh_python from debian/rules

 -- Kai Hendry <hendry@iki.fi>  Sun, 20 Jan 2008 13:59:06 +0000

webpy (0.220-1) unstable; urgency=low

  * New upstream release
  * python-webpy: Eats a request/response cycle when using SCGI and the
    client makes a POST request and the application returns a response
    with status 302 (Closes: #429272)

 -- Kai Hendry <hendry@iki.fi>  Mon, 27 Aug 2007 22:23:59 +0100

webpy (0.210-1) unstable; urgency=medium

  * New upstream release
  * new upstream release with security fix (Closes: #427715)
  * Upstream announcement:
    http://groups.google.com/group/webpy/browse_thread/thread/c97f397c91a52996
  * Updated copyright to reflect wsgiserver inclusion
  * http://webpy.org/changes

 -- Kai Hendry <hendry@iki.fi>  Wed, 06 Jun 2007 08:45:06 +0100

webpy (0.200-1) unstable; urgency=low

  * New upstream release
  * Added a NEWS file hopefully warning people about this upgrade
  * Removed my setup.py for upstream's
  * Requires python2.4 or greater

 -- Kai Hendry <hendry@iki.fi>  Tue, 28 Nov 2006 19:18:34 -0300

webpy (0.138-2) unstable; urgency=low

  * Update for new Python policy
  * Python policy transition (Closes: #373407)
  * Thanks again to Sanghyeon Seo and Fabio Tranchitella
  * Thanks to Josselin Mouette

 -- Kai Hendry <hendry@iki.fi>  Tue, 27 Jun 2006 12:17:01 +0900

webpy (0.138-1) unstable; urgency=low

  * New upstream release

 -- Kai Hendry <hendry@iki.fi>  Tue,  9 May 2006 22:52:48 +0900

webpy (0.137-2) unstable; urgency=low

  * Support for all versions of the interpreter with python-support
  * Thanks to Josselin Mouette and Sanghyeon Seo

 -- Kai Hendry <hendry@iki.fi>  Tue,  9 May 2006 18:07:29 +0900

webpy (0.137-1) unstable; urgency=low

  * New upstream release
  * template.py added
  * non-non-free, it's now all in the Public Domain!

 -- Kai Hendry <hendry@iki.fi>  Wed, 29 Mar 2006 13:36:26 +0900

webpy (0.135-1) unstable; urgency=low

  * New upstream release

 -- Kai Hendry <hendry@iki.fi>  Wed, 22 Feb 2006 17:54:21 +0900

webpy (0.133-1) unstable; urgency=low

  * New upstream release

 -- Kai Hendry <hendry@iki.fi>  Mon, 13 Feb 2006 17:58:34 +0900

webpy (0.13-1) unstable; urgency=low

  * Initial release. (Closes: #349763)

 -- Kai Hendry <hendry@iki.fi>  Mon,  6 Feb 2006 11:51:49 +0900
